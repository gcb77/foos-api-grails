FROM tomcat:8

# Remove default apps
RUN rm -r /usr/local/tomcat/webapps/*

# Copy the app as ROOT.war to make it the default app
COPY ./app.war /usr/local/tomcat/webapps/ROOT.war

EXPOSE 8080

CMD ["catalina.sh", "run"]