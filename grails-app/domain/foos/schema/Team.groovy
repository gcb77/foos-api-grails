package foos.schema

class Team {

    static graphql = true

    static hasMany = [players: Player]
    static belongsTo = [event: Event]

    //Concatenate player guids sorted in alphabetical order using '_'
    String teamKey

    static constraints = {
    }

    static mapping = {
        teamKey index: 'team_key_idx'
    }


    String toString() {
        return players.toString()
    }
}
