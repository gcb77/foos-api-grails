package foos.schema

class Event {
    String name
    Date start
    static belongsTo = [tournament: Tournament]
    static hasMany = [teams: Team, matches: Match]

    int teamCount = 0
    int matchCount = 0

    static graphql = true

    static constraints = {
    }

    String toString() {
        "${name}"
    }
}
