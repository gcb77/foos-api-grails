package foos.schema

class Player {

    static graphql = true
    static hasMany = [playerMatches: PlayerMatch]

    String guid
    String playerName

    static constraints = {
        playerName unique: true, nullable: false, blank: false
        guid nullable: false, blank: false
    }

    static mapping = {
        playerName index: 'player_name_idx'
        guid index: 'player_guid_idx'
    }

    String toString() {
        return "${playerName}"
    }
}
