package foos.schema

class Match {

    static belongsTo = [event: Event]

    Team team1
    Team team2

    Date matchStart
    Date matchEnd
    String position
    String matchTable

    static constraints = {
        matchStart nullable: true
        matchEnd nullable: true
    }

    static mapping = {
        table "foos_match"
    }

    String toString() {
        "${team1} vs ${team2}"
    }
}
