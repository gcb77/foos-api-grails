package foos.schema

class Tournament {
    static graphql = true

    static hasMany = [events: Event]
    String guid
    String name
    int playerCount = 0
    int matchCount = 0
    int teamCount = 0
    Date date

    static constraints = {
        guid nullable: false, blank: false
        date nullable: true
    }

    String toString() {
        return "${name} [${guid}]"
    }
}
