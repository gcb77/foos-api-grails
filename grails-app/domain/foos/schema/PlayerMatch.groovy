package foos.schema

class PlayerMatch {
    static graphql = true

    static belongsTo = [player: Player]

    Match match
    Team team

    static constraints = {
    }

    String toString() {
        return "${player.playerName} ${team} ${match.position} ${match.event.name} ${match.event.tournament.name}"
    }
}
