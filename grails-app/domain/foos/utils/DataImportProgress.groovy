package foos.utils

class DataImportProgress {

    Date started
    Date completed
    String tournamentName
    String stage

    static constraints = {
    }
}
