package foos.schema

import foos.utils.DataImportProgress
import grails.converters.JSON
import grails.gorm.transactions.Transactional
import grails.transaction.NotTransactional

@Transactional
class DataImportService {

    def updateCounts(Tournament tournament) {
        def players = [:]
        def teams = [:]
        int matchCount = 0
        tournament.events?.each { event ->
            event.teamCount = event.teams.size()
            event.matchCount = event.matches.size()
            event.teams.each { team ->
                teams[team.teamKey] = true
                team.players.each { player ->
                    players[player.guid] = true
                }
            }
            matchCount += event.matchCount
            event.save()
        }
        tournament.matchCount = matchCount
        tournament.playerCount = players.keySet().size()
        tournament.teamCount = teams.keySet().size()
    }

    def loadTournamentFromFiles(tsFile, matchesFile) {
        def tsLines = tsFile.readLines()
        def tJson = JSON.parse(tsLines[0])

        def stats = [:]

        def lockRecord = DataImportProgress.first()
        if(!lockRecord) {
            lockRecord = new DataImportProgress(started: new Date(), stage: 'starting', tournamentName: tJson.tournament_name)
            lockRecord.save(flush: true)
        }


        Tournament tournament = Tournament.findOrCreateByName(tJson.tournament_name)
        if(!tournament.id) {
            tournament.guid = UUID.randomUUID().toString()
            stats['new tournaments'] = 1 + (stats['new tournaments'] ?: 0)
            if(!tournament.save(flush: true)) {
                println(tournament.errors.allErrors)
            }
        }

        println("Loading tournament ${tJson.tournament_name}")

        lockRecord.stage = 'matches'
        lockRecord.save(flush: true)

        parseMatches(tournament, matchesFile, stats)

        lockRecord.stage = 'complete'
        lockRecord.completed = new Date()
        lockRecord.save(flush: true)

        println("Parse complete, stats: ${stats}")
        return tournament
    }

    static def splitPlayers(String team) {
        return team.split(' & ')
    }

    Player findPlayer(name, stats) {
        Player p = Player.findOrCreateByPlayerName(name)
        if(!p.id || !p.guid) {
            stats['new players'] = 1 + (stats['new players'] ?: 0)
            p.guid = UUID.randomUUID()
            if(!p.save()) {
                println p.errors.allErrors
            }
        } else {
            stats['found players'] = 1 + (stats['found players'] ?: 0)
        }
        return p
    }

    def getTeamFromPlayers(List<Player> players, Event ev, Map stats = [:]) {
        //Create a team from the players (unless one exists)
        def teamKey = players.guid.sort().join('_')
        Team t = Team.findOrCreateByTeamKeyAndEvent(teamKey, ev)
        if(!t.id) {
            stats['new teams'] = 1 + (stats['new teams'] ?: 0)
            //New team object, associate to players
            players.each {
                if(t.players) {
                    t.players << it
                } else {
                    t.players = [it]
                }
            }
        }

        if(!t.save()) {
            println(t.errors.allErrors)
        }
        return t
    }

    def parseMatches(Tournament tournament, File matchesFile, Map stats = [:]) {
        def tData = [
                players: [:],
                events: [:],
                matches: []
        ]

        //Parse all matches and create players in DB
        matchesFile.readLines().each { line ->
            def matchInfo = JSON.parse(line)
            matchInfo.team1Players = []
            matchInfo.team2Players = []
            splitPlayers(matchInfo.team1).collect { name ->
                if(!tData.players[name]) {
                    tData.players[name] = findPlayer(name, stats)
                }
                matchInfo.team1Players << tData.players[name]
            }
            splitPlayers(matchInfo.team2).each { name ->
                if(!tData.players[name]) {
                    tData.players[name] = findPlayer(name, stats)
                }
                matchInfo.team2Players << tData.players[name]
            }
            tData.matches.push(matchInfo)
        }

        //Find or create all events
        for(match in tData.matches) {
            if(!tData.events[match.event]) {
                def ev = Event.findOrCreateByTournamentAndName(tournament, match.event)
                def matchStart = new Date(match.startTime)
                if (!ev.id || !ev.start || ev.start > matchStart) {
                    ev.start = matchStart
                    if (!ev.save(flush: true)) {
                        println(ev.errors.allErrors)
                    }
                }
                tData.events[match.event] = ev
            }
        }

        //Find or create all teams
        for(match in tData.matches) {
            def team1 = getTeamFromPlayers(match.team1Players, tData.events[match.event], stats)
            def team2 = getTeamFromPlayers(match.team2Players, tData.events[match.event], stats)
            match['team1Domain'] = team1
            match['team2Domain'] = team2
        }

        //Create all match entries
        for(match in tData.matches) {
            def m = Match.findOrCreateWhere(
                    team1: match.team1Domain,
                    team2: match.team2Domain,
                    position: match.forPosition?.trim(),
                    matchTable: Integer.parseInt(match.table.trim()).toString(),
                    event: tData.events[match.event],
            )
            if(!m.id) {
                m.matchStart = tData.events[match.event].start
                m.matchEnd = new Date(match.endTime)
                if(!m.save()) {
                    println(m.errors.allErrors)
                }
                stats['new matches'] = 1 + (stats['new matches'] ?: 0)
            } else {
                stats['found matches'] = 1 + (stats['found matches'] ?: 0)
            }
            match.matchDomain = m

        }

        for(match in tData.matches) {
            for(player in match.team1Players) {
                PlayerMatch.findOrSaveByPlayerAndMatchAndTeam(player, match.matchDomain, match.team1Domain)
            }
            for(player in match.team2Players) {
                PlayerMatch.findOrSaveByPlayerAndMatchAndTeam(player, match.matchDomain, match.team2Domain)
            }
        }
    }

    @NotTransactional
    def importTournament(String path) {
        def f = new File(path)
        f.listFiles().each { tLocation ->
            if(tLocation.isDirectory()) {
                def tFiles = tLocation.listFiles()
                def fileNames = tFiles.name
                int tsIndex = fileNames.indexOf('tournamentStats.db')
                int mIndex = fileNames.indexOf('matchesInProgress.db')
                if(tsIndex >= 0 && mIndex >= 0) {
                    Tournament tournament = loadTournamentFromFiles(tFiles[tsIndex], tFiles[mIndex])
                    updateCounts(tournament)
                } else {
                    println("None for " + tLocation.path)
                }
            }
        }
        return true
    }

}
